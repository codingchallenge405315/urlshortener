# Coding Challenge - Url Shortener Service


## Prerequisites

Docker and Docker Compose installed on your system.

## Getting Started

run 
```http
  docker-compose up -d
```
This will start both the backend service and postgres database. You can then test the endpoints via Postman or any other HTTP client.

If you want to launch just the database and debug the project in your IDE run:
```http
  docker-compose up db -d
```
or just the backend service:

```http
  docker-compose up backend -d
```


## API Reference

#### Resolve Long Url From Short Link

```http
  GET /api/v1/urls/{shortLink}
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `shortLink` | `String` | **Required** |

#### POST Create short link from long url

```http
  POST /api/v1/urls/short
```

| RequestBody | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `Json Object`     | `Json` | **Required**. |


Json Example:
```http
{
  "original_url": "https://google.com/testing/my/long/url"
}
```