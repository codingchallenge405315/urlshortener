FROM maven:3.8.6-eclipse-temurin-17 AS build
WORKDIR /Urlshortener
COPY pom.xml ./
COPY src ./src
RUN mvn package -DskipTests

FROM maven:3.8.6-eclipse-temurin-17
WORKDIR /Urlshortener
COPY --from=build /Urlshortener/target/*.jar ./Urlshortener.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "Urlshortener.jar"]