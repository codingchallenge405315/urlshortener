package com.codingchallenge.urlshortener.controller;

import com.codingchallenge.urlshortener.domain.dto.ShortUrlRequestV1;
import com.codingchallenge.urlshortener.domain.dto.ShortUrlResponseV1;
import com.codingchallenge.urlshortener.service.ShortUrlService;
import com.google.common.base.Strings;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.google.common.base.Preconditions.checkArgument;

@RestController
@RequestMapping("/api/v1")
public class ShortUrlController {

    private ShortUrlService shortUrlService;

    public ShortUrlController(ShortUrlService shortUrlService){
        this.shortUrlService = shortUrlService;
    }

    /**
     * Generates a short URL for the provided long URL and persists it in the system.
     *
     * @param shortUrlRequestV1 The request object containing the long URL to generate a short URL for.
     * @return ResponseEntity containing the generated short URL.
     *         Returns HTTP status 201 (Created) if the short URL is successfully generated and persisted.
     * @throws IllegalArgumentException if the provided request object is null.
     */
    @PostMapping(value = "/urls/short", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> generateAndPersistShortUrl(@RequestBody ShortUrlRequestV1 shortUrlRequestV1){
        checkArgument(shortUrlRequestV1 != null);
        checkArgument(!Strings.isNullOrEmpty(shortUrlRequestV1.getOriginalUrl()));

        ShortUrlResponseV1 shortUrlResponseV1 = shortUrlService.generateAndPersistShortUrl(shortUrlRequestV1);

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(shortUrlResponseV1);
    }

    /**
     * Retrieves the full URL associated with the given short link.
     *
     * @param shortLink The short link to resolve to a full URL.
     * @return ResponseEntity containing the full URL associated with the short link.
     *         Returns HTTP status 200 (OK) if the short link is found and the full URL is resolved.
     *         Returns an empty response body if the short link is not found.
     * @throws IllegalArgumentException if the provided short link is null or empty.
     */
    @GetMapping(value = "/urls/{shortLink}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Cacheable(value = "resolveFullUrl", key = "#shortLink")
    public ResponseEntity<?> resolveFullUrl(@PathVariable final String shortLink){
        checkArgument(!Strings.isNullOrEmpty(shortLink));

        ShortUrlResponseV1 shortUrlResponseV1 = shortUrlService.getEncodedUrl(shortLink);

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(shortUrlResponseV1);
    }


}
