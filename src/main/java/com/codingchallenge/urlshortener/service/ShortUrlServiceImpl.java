package com.codingchallenge.urlshortener.service;

import com.codingchallenge.urlshortener.domain.dto.ShortUrlRequestV1;
import com.codingchallenge.urlshortener.domain.dto.ShortUrlResponseV1;
import com.codingchallenge.urlshortener.domain.entities.ShortUrlEntity;
import com.codingchallenge.urlshortener.repository.ShortUrlRepository;
import com.codingchallenge.urlshortener.util.EncodeUtil;
import org.apache.commons.validator.routines.UrlValidator;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.google.common.base.Preconditions.checkArgument;

@Service
public class ShortUrlServiceImpl implements ShortUrlService{

    private ShortUrlRepository shortUrlRepository;

    public ShortUrlServiceImpl(ShortUrlRepository shortUrlRepository){
        this.shortUrlRepository = shortUrlRepository;
    }

    @Override
    public ShortUrlResponseV1 generateAndPersistShortUrl(ShortUrlRequestV1 shortUrlRequestV1) {
        checkArgument(shortUrlRequestV1 != null);
        checkArgument(isValidUrl(shortUrlRequestV1.getOriginalUrl()));

        String encodedUrl = EncodeUtil.encodeUrl(shortUrlRequestV1.getOriginalUrl());
        ShortUrlEntity shortUrlEntityToBePersisted = new ShortUrlEntity();
        shortUrlEntityToBePersisted.setLongUrl(shortUrlRequestV1.getOriginalUrl());
        shortUrlEntityToBePersisted.setShortUrl(encodedUrl);

        ShortUrlEntity persistedShortUrlEntity = persistShortUrl(shortUrlEntityToBePersisted);

        checkArgument(persistedShortUrlEntity != null);

        ShortUrlResponseV1 shortUrlResponseV1 = new ShortUrlResponseV1();
        shortUrlResponseV1.setLongUrl(persistedShortUrlEntity.getLongUrl());
        shortUrlResponseV1.setShortUrl(persistedShortUrlEntity.getShortUrl());

        return shortUrlResponseV1;
    }

    @Override
    public ShortUrlResponseV1 getEncodedUrl(String url) {
        ShortUrlEntity shortUrlEntity = shortUrlRepository.findByShortUrl(url);

        checkArgument(shortUrlEntity != null);

        ShortUrlResponseV1 shortUrlResponseV1 = new ShortUrlResponseV1();
        shortUrlResponseV1.setShortUrl(shortUrlEntity.getShortUrl());
        shortUrlResponseV1.setLongUrl(shortUrlEntity.getLongUrl());

        return shortUrlResponseV1;
    }

    @Override
    public ShortUrlEntity persistShortUrl(ShortUrlEntity shortUrlEntity) {
        ShortUrlEntity persistedShortUrlEntity = shortUrlRepository.save(shortUrlEntity);
        Pageable pageable = PageRequest.of(0, 4);
        Page<ShortUrlEntity> resultPage = shortUrlRepository.findAll(pageable);
        List<ShortUrlEntity> pageResult = resultPage.getContent();

        return persistedShortUrlEntity;
    }



    private boolean isValidUrl(String url) {
        UrlValidator validator = new UrlValidator();
        return validator.isValid(url);
    }

}
