package com.codingchallenge.urlshortener.service;

import com.codingchallenge.urlshortener.domain.dto.ShortUrlRequestV1;
import com.codingchallenge.urlshortener.domain.dto.ShortUrlResponseV1;
import com.codingchallenge.urlshortener.domain.entities.ShortUrlEntity;

public interface ShortUrlService {

    ShortUrlResponseV1 generateAndPersistShortUrl(ShortUrlRequestV1 shortUrlRequestV1);
    ShortUrlResponseV1 getEncodedUrl(String url);
    ShortUrlEntity persistShortUrl(ShortUrlEntity shortUrlEntity);

}
