package com.codingchallenge.urlshortener.util;

import com.google.common.hash.Hashing;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;

public final class EncodeUtil {

    private EncodeUtil(){
    }

    public static String encodeUrl(String originalUrl){
        String encodedUrl="";
        LocalDateTime time = LocalDateTime.now();
        encodedUrl = Hashing.murmur3_32_fixed()
                .hashString(originalUrl.concat(time.toString()), StandardCharsets.UTF_8)
                .toString();
        return encodedUrl;
    }
}
