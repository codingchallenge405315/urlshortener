package com.codingchallenge.urlshortener.domain.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(indexes = {@Index(name = "idx_short_url", columnList="short_url")})
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ShortUrlEntity {

    @Id
    @GeneratedValue( strategy = GenerationType.SEQUENCE )
    private Long id;

    @Column(name="long_url")
    private String longUrl;

    @Column(name="short_url")
    private String shortUrl;

}
