package com.codingchallenge.urlshortener.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Getter
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class ShortUrlRequestV1 {

    @JsonProperty(value = "original_url", required = true)
    private final String originalUrl;

}
