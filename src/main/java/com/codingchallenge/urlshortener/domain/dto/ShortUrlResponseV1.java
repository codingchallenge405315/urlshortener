package com.codingchallenge.urlshortener.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ShortUrlResponseV1 {

    @JsonProperty(value = "long_url")
    private String longUrl;

    @JsonProperty(value = "short_url")
    private String shortUrl;
}
