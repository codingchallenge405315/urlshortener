package com.codingchallenge.urlshortener.repository;

import com.codingchallenge.urlshortener.domain.entities.ShortUrlEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShortUrlRepository extends JpaRepository<ShortUrlEntity, Long> {

    ShortUrlEntity findByShortUrl(String shortUrl);

}
