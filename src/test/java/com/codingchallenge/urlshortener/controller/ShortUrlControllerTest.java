package com.codingchallenge.urlshortener.controller;

import com.codingchallenge.urlshortener.domain.dto.ShortUrlRequestV1;
import com.codingchallenge.urlshortener.domain.entities.ShortUrlEntity;
import com.codingchallenge.urlshortener.repository.ShortUrlRepository;
import com.codingchallenge.urlshortener.util.EncodeUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@AutoConfigureMockMvc
class ShortUrlControllerTest {

    private MockMvc mockMvc;

    private ObjectMapper objectMapper;

    private ShortUrlRepository urlRepository;

    @Autowired
    public ShortUrlControllerTest(MockMvc mockMvc, ObjectMapper objectMapper,ShortUrlRepository urlRepository){
        this.mockMvc = mockMvc;
        this.objectMapper = objectMapper;
        this.urlRepository = urlRepository;
    }

    @Test
    public void testThatCreateShortUrlSuccessfullyReturnsHttp201Created() throws Exception {
        ShortUrlRequestV1 shortUrlRequestV1 = new ShortUrlRequestV1(
                "https://www.googe.com/my-test-is-great");

        String requestJson = objectMapper.writeValueAsString(shortUrlRequestV1);

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/urls/short")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.long_url").value(shortUrlRequestV1.getOriginalUrl()))
                .andExpect(jsonPath("$.short_url").isNotEmpty());
    }

    @Test
    public void testThatResolveLongUrlSuccesfullyReturnsHttp200Ok() throws Exception {

        ShortUrlEntity shortUrlEntity = new ShortUrlEntity();
        shortUrlEntity.setLongUrl("http://www.thisIsTheBestTest.com");
        shortUrlEntity.setShortUrl(EncodeUtil.encodeUrl("http://www.thisIsTheBestTest.com"));
        urlRepository.save(shortUrlEntity);


        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/urls/"+shortUrlEntity.getShortUrl())
                        .contentType(MediaType.APPLICATION_JSON)
                        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.long_url").value(shortUrlEntity.getLongUrl()))
                .andExpect(jsonPath("$.short_url").value(shortUrlEntity.getShortUrl()));

    }

}